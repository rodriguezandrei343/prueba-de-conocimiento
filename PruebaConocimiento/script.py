def Creciente(numero):
    # Determina cuando un numero es creciente,  devuelve True si es creciente
    aux = True
    numeroStr = str(numero)
    for i in range(len(numeroStr) -1):
        if int(numeroStr[i]) > int(numeroStr[i+1]):
            aux = False
            return aux
    return aux


def Decreciente(numero):
    # Determina cuando un numero es decreciente,  devuelve True si es decreciente
    aux = True
    numeroStr = str(numero)
    for i in range(len(numeroStr) -1):
        if int(numeroStr[i]) < int(numeroStr[i+1]):
            aux = False
            return aux
    return aux


def main():
    # Funcion principal, realizan calculos para determinar la porporcion de numeros zigzagueantes
    cont = 99
    zigzagueante = 0
    porcentajeZigzagueante = 0.0
    while int(porcentajeZigzagueante) != 99:
        cont+=1
        if Creciente(cont) == False and Decreciente(cont) == False:
            zigzagueante +=1
        porcentajeZigzagueante = (zigzagueante*100.0)/cont
    print ('Numero menor zigzagueantes es: ', cont, ' Porcentaje: ', porcentajeZigzagueante)


if __name__ == '__main__':
    main()